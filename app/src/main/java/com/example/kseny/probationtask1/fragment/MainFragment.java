package com.example.kseny.probationtask1.fragment;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.kseny.probationtask1.MainActivity;
import com.example.kseny.probationtask1.R;
import com.example.kseny.probationtask1.adapter.RecyclerAdapter;
import com.example.kseny.probationtask1.listener.CustomListener;
import com.example.kseny.probationtask1.util.FontUtil;

import java.util.Arrays;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Ksyu on 19.03.2016.
 */
public class MainFragment extends Fragment {

    private RecyclerAdapter adapter;
    private List<String> uriList;
    private MainActivity activity;

    private Context context;

    @Bind(R.id.title)
    public TextView title;

    @Bind(R.id.status)
    public TextView status;

    @Bind(R.id.date_creation_label)
    public TextView date_creation_label;

    @Bind(R.id.date_creation)
    public TextView date_creation;

    @Bind(R.id.date_registration_label)
    public TextView date_registration_label;

    @Bind(R.id.date_registration)
    public TextView date_registration;

    @Bind(R.id.date_deadline_label)
    public TextView date_deadline_label;

    @Bind(R.id.date_deadline)
    public TextView date_deadline;

    @Bind(R.id.executive_label)
    public TextView executive_label;

    @Bind(R.id.executive_name)
    public TextView executive_name;

    @Bind(R.id.main_text)
    public TextView main_text;

    @Bind(R.id.image_recycler_view)
    public RecyclerView recycler_view;


    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        context=getContext();//getActivity().getApplicationContext();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View containerView = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this, containerView);

        CustomListener listener = new CustomListener(context);

        setTextViewText();
        setTextViewListener(listener);
        setRobotoFont();

        uriList= Arrays.asList(getResources().getStringArray(R.array.images));

        LinearLayoutManager layoutManager
                = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);

        recycler_view.setLayoutManager(layoutManager);

        adapter = new RecyclerAdapter(uriList, context);
        recycler_view.setAdapter(adapter);
        recycler_view.setItemAnimator(new DefaultItemAnimator());

        return containerView;
    }

    private void setTextViewText() {

        title.setText(R.string.title);
        status.setText(R.string.status);
        date_creation_label.setText(R.string.create_label);
        date_creation.setText(R.string.date_creation);
        date_registration_label.setText(R.string.register_label);
        date_registration.setText(R.string.date_registration);
        date_deadline_label.setText(R.string.date_deadline_label);
        date_deadline.setText(R.string.date_deadline);
        executive_label.setText(R.string.executive_label);
        executive_name.setText(R.string.executive_name);
        main_text.setText(R.string.main_text);
    }

    private void setTextViewListener(CustomListener listener) {

        title.setOnClickListener(listener);
        status.setOnClickListener(listener);
        date_creation_label.setOnClickListener(listener);
        date_creation.setOnClickListener(listener);
        date_registration_label.setOnClickListener(listener);
        date_registration.setOnClickListener(listener);
        date_deadline_label.setOnClickListener(listener);
        date_deadline.setOnClickListener(listener);
        executive_label.setOnClickListener(listener);
        executive_name.setOnClickListener(listener);
        main_text.setOnClickListener(listener);

    }

    private void setRobotoFont(){

        title.setTypeface(FontUtil.getRobotoBoldTypeFace(context));
        status.setTypeface(FontUtil.getRobotoMediumTypeFace(context));
        date_creation_label.setTypeface(FontUtil.getRobotoMediumTypeFace(context));
        date_creation.setTypeface(FontUtil.getRobotoMediumTypeFace(context));
        date_registration_label.setTypeface(FontUtil.getRobotoMediumTypeFace(context));
        date_registration.setTypeface(FontUtil.getRobotoMediumTypeFace(context));
        date_deadline_label.setTypeface(FontUtil.getRobotoMediumTypeFace(context));
        date_deadline.setTypeface(FontUtil.getRobotoMediumTypeFace(context));
        executive_label.setTypeface(FontUtil.getRobotoMediumTypeFace(context));
        executive_name.setTypeface(FontUtil.getRobotoMediumTypeFace(context));
        main_text.setTypeface(FontUtil.getRobotoMediumTypeFace(context));

    }
}
