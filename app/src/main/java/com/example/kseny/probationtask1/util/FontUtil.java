package com.example.kseny.probationtask1.util;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by Ksyu on 20.03.2016.
 */
public class FontUtil {

    private static Typeface fontBold;
    private static Typeface fontMedium;

    public static Typeface getRobotoBoldTypeFace(Context context) {
        if (fontBold == null) {
            fontBold = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Bold.ttf");
        }
        return fontBold;
    }
    public static Typeface getRobotoMediumTypeFace(Context context) {
        if (fontMedium == null) {
            fontMedium = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Medium.ttf");
        }
        return fontMedium;
    }
}
