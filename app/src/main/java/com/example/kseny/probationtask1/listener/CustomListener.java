package com.example.kseny.probationtask1.listener;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

/**
 * Created by Ksyu on 19.03.2016.
 */
public class CustomListener implements View.OnClickListener {

    private Context context;

    String control_id;
    String control_name;

    public CustomListener(Context context) {
        this.context = context;
    }

    @Override
    public void onClick(View v) {

        control_id = v.getResources().getResourceName(v.getId());
        control_name = control_id.substring(control_id.lastIndexOf("/") + 1);
        Toast.makeText(context, control_name, Toast.LENGTH_SHORT).show();
        Log.d("listener", control_name);
    }
}
